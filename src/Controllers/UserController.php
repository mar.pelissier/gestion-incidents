<?php

namespace BugApp\Controllers;

use BugApp\Models\UserManager;  // use aide l'application à savoir où se trouve le fichier
use BugApp\Models\RecorderManager;
use BugApp\Models\Engineer;
use BugApp\Models\Recorder;
use BugApp\Controllers\abstractController;

class UserController extends abstractController  
{

    public function login()
    {

        if(!empty($_POST)){


        // Si il existe des données postées,
        // vérifier que le login exite en base de données aux données saisies en base de données
        $email = $_POST['email'];
        
        $manager = new UserManager();

        $user = $manager->findByEmail($email);

            if($user !== null){
            
            // Si oui : Vérifier que le mot de passe correspond
            
            $password = $_POST['password'];

            $check = $manager -> check($user, $password);

                if($check === true ){
                    // Si oui :
                    switch(get_class($user)){

                        case 'BugApp\Models\Recorder':

                        // Si l'utilisateur est un 'recorder', alors :
                        
                        // - créer une session                      
                        $_SESSION['user'] = $user;
                        $_SESSION['type'] = Recorder::NAME;
                        
                        // - afficher la liste des incidents (vue Client)
                    
                        header('Location:'.PUBLIC_PATH.'bug');
                    
                        break;  

                        case 'BugApp\Models\Engineer':

                            // Si l'utilisateur est un 'ingénieur', alors :
                            
                            // - créer une session                      
                            $_SESSION['user'] = $user;
                            $_SESSION['type'] = Engineer::NAME;
                            
                            // - afficher la liste des incidents (vue Ingenieur)
                        
                            header('Location:'.PUBLIC_PATH.'bug');
                        
                        break;                 
                    }
                }else{

                    // Si non (le mot de passe ne correspond pas) :
    
                    $error = "Le mot de passe ne correspond pas";
    
                    // Il y a une erreur. Afficher le formulaire de login avec un commentaire
    
                    $content = $this->render('src/Views/User/login', ['error' => $error]);
    
                    return $this->sendHttpResponse($content, 200);
    
                    }
    
                }else{
    
                     // Si non (le login n'existe pas) :
                    $error = "L'email n'existe pas";
    
                    // Il y a une erreur. Afficher le formulaire de login avec un commentaire
    
                    $content = $this->render('src/Views/User/login', ['error' => $error]);
    
                    return $this->sendHttpResponse($content, 200);           
    
                }
              
    
            }else{
    
                // Si non (pas de données postées
                // Afficher simplement le formulaire
    
                $content = $this->render('src/Views/User/login', []);
    
                return $this->sendHttpResponse($content, 200);
    
            }
    }


    public function logout()
    {
        session_destroy();
        unset($_SESSION);
        header('Location:'.PUBLIC_PATH.'login');
        
    }

}
