<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $template_path = 'src/Views/Bug/'.$_SESSION['type'].'/show';

        $content = $this->render($template_path, ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        if(!isset($_SESSION['user'])){

            header('Location: ' . PUBLIC_PATH .'login');

        }
        $bugs = [];

        // TODO: liste des incidents

        
        // Données issues du Modèle

        $manager = new BugManager();

        $bugs = $manager->findAll();

        $template_path = 'src/Views/Bug/'.$_SESSION['type'].'/list';

        $content = $this->render($template_path, ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident

        if (isset($_POST['submit'])) {

            $bugManager = new BugManager();

            $bug = new Bug();
            $bug->setTitle($_POST["title"]);
            $bug->setDescription($_POST["description"]);

            $dt = new \DateTime($_POST["createdAt"]);
            $dtToString = $dt->format('Y-m-d H:i:s');
            $bug->setCreatedAt($dtToString);

            $bugManager->add($bug);

            header('Location: ' . PUBLIC_PATH .'bug');

        } else {

            $content = $this->render('src/Views/Bug/Recorder/add', []);

            return $this->sendHttpResponse($content, 200);
        }

        // TODO: ajout d'incident (GET et POST)

        $content = $this->render('src/Views/Bug/add', []);

        return $this->sendHttpResponse($content, 200);
    }

    public function update($id){

        // Update d'un incident
        $manager = new BugManager();
        $bug = $manager ->find($id);
  
        if(isset($_POST['sendIt'])){
  
            if(isset($_POST['cloture'])) {
  
                $manager->update($bug);
                header('Location:'.PUBLIC_PATH.'bug/show/'.$id);
  
            }
            else{
                header('Location:'.PUBLIC_PATH.'bug/update/'.$id);
            }
        }
        else{

        $template_path = 'src/Views/Bug/'.$_SESSION['type'].'/update';
        $content = $this->render($template_path, ['bug' => $bug]);
        return $this->sendHttpResponse($content, 200);

        }
  
     }

     public function assign($id){

        $manager = new BugManager();
        $bug = $manager ->find($id);
        if($bug->getEngineer()==null){
            $user=$_SESSION['user'];
            $manager->assign($bug, $user->getId());
        }
        
     }
     
    public function close($id){

        $manager = new BugManager();
        $bug = $manager ->find($id);
        if($bug->getCloseAt()==null){
            $manager->close($bug)->format("d/m/Y");
        }

     }



     
}
