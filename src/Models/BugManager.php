<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["recorder_id"]);
        $bug->setEngineer($result["engineer_id"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Récupération de tous les incidents en BDD

        $bugs = [];

        // Requête
        $sth = $dbh->prepare('SELECT * FROM `bug` ORDER BY `id`');
        $sth->execute();
        while ( $result = $sth->fetch(\PDO::FETCH_ASSOC)){
            $bug = new Bug();
            $bug->setId($result["id"]);
            $bug->setTitle($result["title"]);
            $bug->setDescription($result["description"]);
            $bug->setCreatedAt($result["createdAt"]);
            $bug->setClosedAt($result["closed"]);
            $bugs[] = $bug;

        }
        return $bugs;
    }

    public function add(Bug $bug){

        $dbh = static::connectDb();

        $sql = "INSERT INTO bug (title, description, closed, createdAt) VALUES (:title, :description, :closed, :createdAt)";
        $sth = $dbh->prepare($sql);
        $sth->execute([
            "title" => $bug->getTitle(),
            "description" => $bug->getDescription(),
            "createdAt" => $bug->getCreatedAt()->format('Y-m-d H:i:s'),
            "closed" => null,
        ]);

    }

    public function update(Bug $bug){

       // Modification d'un incident en BDD

       $dbh = static::connectDb();

       $req = $dbh->prepare('UPDATE bug
                             SET closed = :clotureDate
                             WHERE id = '.$bug->getId()
                           );

       $req->execute(array(

              'clotureDate' => date("Y-m-d H:i:s")

       ));

    }

    public function assign(Bug $bug, $engineer_id){

        // Modification d'un incident en BDD
 
        $dbh = static::connectDb();
 
        $req = $dbh->prepare('UPDATE bug
                              SET engineer_id = (SELECT id FROM engineer WHERE user_id = :engineer_id)
                              WHERE id = '.$bug->getId()
                            );
 
        $req->execute(array(
 
               'engineer_id' => $engineer_id
 
        ));
 
     }
     

    public function close(Bug $bug, $closed){

        // Modification d'un incident en BDD
 
        $dbh = static::connectDb();
 
        $req = $dbh->prepare('UPDATE bug
                              SET closed = :clotureDate
                              WHERE id = '.$bug->getId()
                            );
 
        $req->execute(array(
 
               'closed' => $closed
 
        ));
 
     }


}
