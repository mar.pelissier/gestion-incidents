<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>

<?php
    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
 ?>


<!DOCTYPE html>

<html>

<body>

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
	<a class="waves-effect waves-light btn" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>Retour</a>
      <br><br>
      <h1 class="header center blue-grey-text">Fiche descriptive d'incident</h1>
	  </br>
	  </hr>
	  </br>
	  <div class= "row">
		<div class="col s6 left">Nom de l'incident : <?=$bug->getTitle();?> </div>  
		<div class="col s6 right">Date de création : <?php echo $bug->getCreatedAt()->format("d/m/Y");?> </div>
		</br> </br> 
		<div class="col s3">Description de l'incident : </div> </br> 
		<?=$bug->getDescription();?>
        </div>
        </br>
        <?php if($bug->getClosedAt() != ""){
        echo '<div class="col s6 right">Date de fermeture :' . $bug->getClosedAt()->format("d/m/Y"). '</div>';
        }
        ?>
        </div>
	  </div>
    </div>
  </div>

</body>
<footer>
    <?php   
        include(__DIR__.'./../../include/footer.php');
    ?>
</footer>

</html>