<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

 <?php
    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
 ?>

<body>

    <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-grey-text">Liste des incidents</h1>
  <a class="btn-floating btn-small waves-effect waves-light blue-grey"><i class="material-icons">add</i></a> <a href="<?=PUBLIC_PATH?>bug/add"> &nbsp;&nbsp; Rapporter un incident </a>
  
	        <table>
        <thead>
          <tr>
              <th>id</th>
              <th>Sujet</th>
              <th>Date</th>
              <th>Cloture</th>
              <th>Description</th>
			  <th>  </th>
          </tr>
        </thead>

        <tbody>


        <?php
        foreach ($bugs as $bug) { ?>
          <tr>
          <td><?=$bug->getId();?></td>
          <td><?=$bug->getTitle();?></td>
          <td><?=$bug->getcreatedAt()->format("d/m/Y");?></td>
          <td><?php 
          if ($bug->getclosedAt() == "") {
            echo "Non clôturé";
          }else{
            echo $bug->getclosedAt();
          } 
          ?></td>
          <td><a href="bug/show/<?=$bug->getId();?>">Afficher</a></td>
        </tr>
        <?php }
        ?>
        </tbody>
      </table>

    </div>
  </div>

  <div class="container">
    <div class="section">

    </div>
    <br><br>
  </div>

</body>

<footer>
    <?php   
        include(__DIR__.'./../../include/footer.php');
    ?>
</footer>

</html>
