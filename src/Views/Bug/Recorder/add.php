<!DOCTYPE html>

<html>

 <?php
    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
 ?>

<body>

  
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
	<a class="waves-effect waves-light btn" href="<?=PUBLIC_PATH?>bug"><i class="material-icons left">arrow_back</i>Retour</a>
      <br><br>
      <h1 class="header center blue-grey-text">Consigner un nouvel incident</h1>  
	  </br>
	  </hr>
	  </br>
	  <form action="<?= PUBLIC_PATH; ?>bug/add" method="POST" >
        <div class="col s6 left">
                Nom de l'incident : <input placeholder="titre" id="titre" type="text" class="validate" name="title">
        </div>
        <div class="col s6 right">Date d'observation :<input placeholder="date"  id="date" type="date" class="validate" name="createdAt">
        </div>
        </br> </br> </br> </br>
        <div class="col s3">Description de l'incident : </div> <div class="col 9s"> 
        <textarea name="description" rows=4 cols=40></textarea>
        </div>
		</p>
		</div>
		<div class="col s9 offset-s3">
		<button class="waves-effect waves-light btn" type="submit" name="submit">Enregistrer</button>
		</div>
	  </form>
	</div>
    </div>
    </div>
  </div>


  <div class="container">
    <div class="section">

    </div>
    <br><br>
  </div>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
<footer>
    <?php   
        include(__DIR__.'./../../include/footer.php');
    ?>
</footer>
</html>
