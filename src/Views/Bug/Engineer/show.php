<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
?>


<!DOCTYPE html>

<html>

    <body>

        <div class="section no-pad-bot" id="index-banner">
            <div class="container">
                <br><br>
                <h1 class="header center blue-grey-text text-darken-4">Fiche d'incident</h1>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-3" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons">arrow_back</i></a>
            </div>
        <div class="section">
            <!--   Info Section   -->
            <div class="row">
                <div class="col s12 m12 l12"><h5>Titre : </h5><div><?=$bug->getTitle();?></div></div>
            </div>
            <div class="row">
                <div class="col s12 m6 l6">
                    <h5>Date d'observation : </h5>
                    <div><?php  echo $bug->getCreatedAt()->format("d/m/Y"); ?></div>
                </div>
                <div class="col s12 m6 l6">
                    <?php if($bug->getClosedAt() != null){ ?>
                    <h5>Date de cloture : </h5>
                    <div><?php  echo $bug->getClosedAt()->format("d/m/Y"); ?> </div>
                    <?php  } ?>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5>Description : </h5>
                    <div>
                        <?=$bug->getDescription();?>
                    </div>
                </div>
            </div>

                <a class="btn-floating btn-small waves-effect waves-light blue-grey"><i class="material-icons"></i></a> <a href="<?=PUBLIC_PATH?>bug/update/<?= $bug->getId() ?>"> &nbsp;&nbsp; Editer </a>
        </div>
        </div>

    </body>
    <footer>
        <?php   
            include(__DIR__.'./../../include/footer.php');
        ?>
    </footer>

</html>