<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

 <?php
    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
 ?>

  <body>

    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center blue-grey-text">Liste des incidents</h1>
    
        <div class="section">

          <div class="row">
              <h5>Filtres</h5>
              <p class="filtre">
                  <label>
                      <input type="checkbox" />
                      <span>Afficher uniquement les incidents non-clôturés.</span>
                  </label>
              </p>
              <p class="filtre">
                  <label>
                      <input type="checkbox" />
                      <span>Afficher uniquement les incidents que je me suis assignés.</span>
                  </label>
              </p>
          </div>

              <table>
                <thead>
                  <tr>
                      <th>id</th>
                      <th>Sujet</th>
                      <th>Date</th>
                      <th>Afficher</th>
                      <th>Assigner</th>
                      <th>Cloturer</th>
                  </tr>
                </thead>
                <tbody>

                <?php foreach ($bugs as $bug) { ?>
                    <?php /** @var $bug \BugApp\Models\Bug */ ?>
                    <tr id="bug_<?= $bug->getId(); ?>" class="bug">
                        <td><?= $bug->getId(); ?></td>
                        <td><?= $bug->getTitle(); ?></td>
                        <td><?= $bug->getCreatedAt()->format('d/m/Y'); ?></td>
                        <td><a href="<?= PUBLIC_PATH; ?>bug/show/<?= $bug->getId(); ?>">Afficher</a></td>
                        <td><a href="<?= PUBLIC_PATH; ?>bug/update/<?= $bug->getId(); ?>&action=assign" id="assigner">Assigner</td>
                        <td>  
                          <?php 
                            if($bug->getClosedAt()!=null){
                              echo $bug->getClosedAt()->format('d/m/Y') ;
                            }else{
                              echo '<a href="<?= PUBLIC_PATH; ?>bug/show/<?= $bug->getId(); ?>&action=close" id="close" >Clôturer</a>' ;
                            }
                          ?>
                        </td>
                          
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  
  </body>

  <footer>
      <?php   
          include(__DIR__.'./../../include/footer.php');
      ?>
  </footer>

  <script src="/Gestion/application-de-gestion-d-incidents/public/js/script.js"></script>
</html>
