<nav class="blue-grey lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Gestion d'incidents</a>
      <ul class="right hide-on-med-and-down">
          <?php
            if(isset($_SESSION['user'])){
              $user = $_SESSION['user'];
            
            ?>
          <li>
            <a href="#">
            <i class="material-icons">person_outline</i><a href="logout" class="user">Deconnexion
              </a>
            </a>
          </li>
          <li>
              <?=($_SESSION['user'])->getNom();?>
          </li>
        <?php
            };
        ?>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#"><i class="material-icons">person_outline</i>Deconnexion</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  