<footer class="page-footer blue-grey lighten-1">
    <div class="container">
      <div class="row">

        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by Marianne Pelissier<a class="orange-text text-lighten-3" href="http://materializecss.com"> | Materialize </a>
      </div>
    </div>
  </footer>