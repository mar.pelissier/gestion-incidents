<?php

if(isset( $parameters['error'])){
    $error = $parameters['error'];
}


include(__DIR__.'./../include/header.php');
include(__DIR__.'./../include/nav.php');


?>

    <body>

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-grey-text text-darken-4">Login</h1>
        </div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row"><?php if(isset($error)) echo $error;?></div>

            <div class="row">
                <form class="col s12" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="email" id="email" type="email" class="validate" name="email">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s6">
                            <input placeholder="password"  id="password" type="password" class="validate" name="password">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button class="btn" type="submit" name="submit">Login
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<footer>
    <?php   
        include(__DIR__.'./../include/footer.php');
    ?>
</footer>