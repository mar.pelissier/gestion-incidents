document.getElementById("assigner").addEventListener('click', makeRequest);

function makeRequest(event_assign) {
    event_assign.preventDefault();
    //test appeler l'event grâce .target :
    //console.log(event_assign.target.href);
    httpRequest = new XMLHttpRequest();  
    httpRequest.onreadystatechange = alertContents;
    httpRequest.open('GET', event_assign.target.href);
    httpRequest.send();
  }

  function alertContents() {
    alert('Lincident à bien été assigné');
  }

