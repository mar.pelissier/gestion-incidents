<?php

//session_start(); 
require('../src/init.php');

session_start();

use BugApp\Controllers\bugController;
use BugApp\Controllers\UserController;

//var_dump($uri);

switch(true) {

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;
    
    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;
    
    case ($uri  == 'login'): 

        $controller = new  UserController();

        return $controller->login();

        break;
    
    case ($uri  == 'logout'): 

        $controller = new  UserController();

        return $controller->logout();

        break ;
        
    case preg_match('#^bug/update/(\d+)$#', $uri, $matches):

        $id = $matches[1];
        
        $controller = new bugController();
   
        return $controller->update($id);
   
        break;

   
    case preg_match('#^bug/update/(\d+)&action=assign$#', $uri, $matches):

        $id = $matches[1];
        
        $controller = new bugController();
   
        return $controller->assign($id);
   
        break;
    

    /*case preg_match('#^bug/update/(\d+)&action=close$#', $uri, $matches):

        $id = $matches[1];
          
        $controller = new bugController();
      
        return $controller->close($id);
       
        break;
        */
        
    default:
    
    http_response_code(404);
    
    echo "<h1>Gestion d'incidents</h1><p>Page par défaut</p>";
    
}